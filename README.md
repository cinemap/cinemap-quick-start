# Cinemap Quick Start

This quick start repo will contain all needed files and configurations to
quickly setup cinemap for evaluations and testing purposes.

## Prerequisites

- Docker incl. compose
- Git

## Preparation

Open a terminal and clone the repository to download all necessary files:

```bash
git clone https://gitlab.com/cinemap/cinemap-quick-start.git
```

Go into the cloned repository:

```bash
cd cinemap-quick-start
```

Open the file `compose.yaml` with your favourite editor and look for
the `repository` service. It should look like this:

```yaml
  repository:
    container_name: cinemap-repository
    image: 'registry.gitlab.com/cinemap/cinemap-repository/cinemap-repository:0.1.0'
    environment:
      - 'JDBC_POSTGRESQL_URL=jdbc:postgresql://repository-db:5432/cinemap'
      - 'DATABASE_PASSWORD=cinemap'
      - 'DATABASE_USER=cinemap'
      - 'TMDB_API_READ_TOKEN=YOUR TOKEN'
      - 'TMDB_API_BASE_URL=https://api.themoviedb.org/3/movie/'
      - 'TMDB_IMG_BASE_URL=https://image.tmdb.org/t/p'
    ports:
      - '8081:8080'
```

Replace the term `YOUR TOKEN` with your TMDB `API Read Access Token`.
Read [here](https://developer.themoviedb.org/docs/getting-started) how to get
one.

Save your edits and close the file.

## Run Cinemap

Make sure the Docker engine is up and running, then execute the following command:

```bash
docker compose up -d
```

The cinemap services should start.

- Check http://localhost:8081 to validate that the repository is running.
- Check http://localhost:8080 to validate that the repository UI is running.

Congratulations! You have successfully started Cinemap!